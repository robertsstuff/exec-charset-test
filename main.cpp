#include <iostream>
#include <iomanip>
#include <string>
#include "sjs/execcharsettest.h"

int main()
{
    auto sjsString = getString();

    if (sjsString == std::string("\x88\xEA\x93\xF1\x8E\x4F\x8E\x6C\x8C\xDC\x98\x5A\x8E\xB5\x94\xAA\x8B\xE3")) {
        std::cout << "Test passed.\n";
    } else {
        std::cout << "Test failed.\n";
    }

    if (sjsString == std::string("一二三四五六七八九")) {
        std::cout << "Charset change was not isolated.\n";
    } else {
        std::cout << "Charset change was isolated.\n";
    }

    std::cout << "Shift-JIS string contents:\n";
    for (auto c: sjsString) {
        std::cout << std::hex << ((int)c & 0xFF) << '\n';
    }

    std::cout << "UTF-8 string contents:\n";
    for (auto c: std::string("一二三四五六七八九")) {
        std::cout << std::hex << ((int)c & 0xFF) << '\n';
    }

    return 0;
}
