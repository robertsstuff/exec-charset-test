# Execution Charset Test

Simple project to test the effect of the exec-charset (or equivalent) options in various compilers.

## Goals

I want to be able to encode a string literal in [Shift-JIS](https://en.wikipedia.org/wiki/Shift_JIS), while the rest of the program's strings are still encoded in UTF-8.

## Methodology

- Apply flags to a library target to enable Shift-JIS encoding.
- Check that the returned string matches the expected string via escaped literals. (Test 1)
- Check that the string DOESN'T match a UTF-8 literal from the main target. (Test 2)

## Compilers Checked

| Compiler      | Flags                    | Did the test compile? | Test 1 | Test 2 |
| ------------- | -----                    | - | - | - |
| GCC (11)      | -fexec-charset=Shift_JIS | Yes | Yes  | Yes |
| Clang (14)    | -fexec-charset=Shift_JIS | No  | N/A  | N/A |
| Clang (15)    | -fexec-charset=Shift_JIS | No  | N/A  | N/A |
| MSVC (19.35)  | /execution-charset:Shift_JIS <br> /source-charset:UTF-8 | Yes | Yes | Yes |
| Clang-CL (14) | /execution-charset:Shift_JIS <br> /source-charset:UTF-8 | No  | N/A | N/A |

## Summary

GCC and MSVC perform as expected.

Clang doesn't support any execution charset besides UTF-8.

Will need to check Clang again if [this feature](https://github.com/llvm/llvm-project/pull/74516) ever gets merged.
