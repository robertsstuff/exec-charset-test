
file(
    GLOB EXEC_CHAR_SOURCES
    execcharsettest.cpp
    execcharsettest.h
)

add_library(sjs STATIC ${EXEC_CHAR_SOURCES})

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    if (CMAKE_CXX_SIMULATE_ID STREQUAL "MSVC")
        target_compile_options(sjs PRIVATE "/execution-charset:Shift_JIS" "/source-charset:UTF-8")
    else()
        target_compile_options(sjs PRIVATE "-fexec-charset=Shift_JIS")
    endif()
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(sjs PRIVATE "-fexec-charset=Shift_JIS")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    target_compile_options(sjs PRIVATE "/execution-charset:Shift_JIS" "/source-charset:UTF-8")
endif()
